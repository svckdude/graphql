import { makeExecutableSchema } from 'graphql-tools';
import { readSchema } from './lib';
import { pingQuery } from './modules/ping/ping';

export const rootSchema = readSchema('./root.gql', __dirname);

export const graphQLSchema = makeExecutableSchema({
  typeDefs: [
    rootSchema
  ],
  resolvers: {
    Query: {
      ping: pingQuery
    }
  }
});
